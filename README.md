**update: if you need to use ES modules in node, try [reify](https://github.com/benjamn/reify) – it's a more complete and well-thought-through project than this one**

# es-module-loader

Node 6 supports [basically all of ES2015](http://node.green/), but you can't use `import` and `export` yet. This fixes that! (Sort of.)

## Usage

To be honest, you probably shouldn't use this (see below). But if you insist:

```bash
npm install es-module-loader
```

```js
// index.js
require( 'es-module-loader' );
assert.equal( require( './foo' ).answer, 42 );

// foo.js
import { multiply } from './utils';
export var answer = multiply( 21, 2 );

// utils.js
export function multiply ( a, b ) {
	return a * b;
}
```

You can also install it globally (`npm install -g es-module-loader`) and run files like so:

```bash
esml-node some-file.js
```


## How it works

`import` and `export` statements are replaced with the equivalent(ish) `require` and `exports` statements, using [tippex](https://github.com/Rich-Harris/tippex).


## Why you probably shouldn't use it

* I made it very quickly as an experiment. It is far from battle-tested
* Default exports are a PITA (see below)
* Live bindings don't work. There are probably other ways this falls short of spec compliance. YMMV!


## Default exports

If the ES module you're requiring has a default export, you need to explicitly refer to it:

```js
// answer.js
export default 42;

// index.js
var answer = require( './answer' );

assert.equal( answer, 42 ); // nope!
assert.equal( answer.default, 42 ); // works, but ugh
```

(This only applies when you're requiring an ES module from a CommonJS module – ES from ES or CommonJS from ES is fine.)


## License

MIT.
