import buble from 'rollup-plugin-buble';

export default {
	entry: 'src/index.js',
	dest: 'dist/es-module-loader.js',
	format: 'cjs',
	plugins: [ buble() ]
};
