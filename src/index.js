var fs = require( 'fs' );
var path = require( 'path' );
var tippex = require( 'tippex' );

var original = require.extensions[ '.js' ];
var nodeModulesPattern = path.sep === '/' ? /\/node_modules\// : /\\node_modules\\/;

// via https://gist.github.com/pilwon/ff55634a29bb4456e0dd
var importPattern = /\bimport\s+(?:(.+)\s+from\s+)?[\'"]([^"\']+)["\'];?/g;
var defaultPattern = /(?:^(\w+)|\*\s+as\s+\w+,\s*(\w+)|\},\s*(?:\* as \w+,\s*)?(\w+))/;
var namespacePattern = /\*\s+as\s+(\w+)/;
var namedPattern = /\{\s*(.+)\s*\}/;
var aliasPattern = /(\w+)(?:\s*as\s*(\w+))?/;

var exportDeclarationPattern = /\bexport\s+(var|let|const|class)\s+(\w+)/g;
var exportFunctionPattern = /\bexport\s+function\s+(\w+)/g;
var exportNamePattern = /export\s+\{(.+)\};?\r?\n?/g;
var exportDefaultPattern = /export\s+default\s+/;

var INTEROP_REQUIRE = 'esModuleLoaderRequire'; // TODO some way to ensure no conflict
var INTEROP_TEMP = 'esModuleLoaderTemp';
var INTEROP = `
function ${INTEROP_REQUIRE} ( source ) {
	var m = require( source );
	return m && m.__esModule ? m.default : m;
}

var ${INTEROP_TEMP};

exports.__esModule = true;
`;

function replaceImports ( match, specifiers, source ) {
	if ( !specifiers ) return `require('${source}');`;

	var statements = [];

	var defau1t = defaultPattern.exec( specifiers );
	if ( defau1t ) {
		statements.push( `var ${defau1t[1] || defau1t[2] || defau1t[3]} = ${INTEROP_REQUIRE}('${source}');` );
	}

	var names = namedPattern.exec( specifiers );
	if ( names ) {
		var statement = `${INTEROP_TEMP} = require('${source}');`;
		names.split( ',' ).forEach( name => {
			name = name.trim();
			if ( !name ) return;

			var alias = aliasPattern.exec( name );

			statement += ` var ${alias[2] || alias[1]} = ${INTEROP_TEMP}.${alias[1]};`;
		});

		statements.push( statement );
	}

	var namespace = namespacePattern.exec( specifiers );
	if ( namespace ) {
		statements.push( `var ${namespace[1]} = require('${source}');` );
	}

	return statements.join( ' ' );
}

require.extensions[ '.js' ] = require.extensions[ '.mjs' ] = function ( m, filename ) {
	// TODO support external modules that expose jsnext:main, or have .mjs extension?
	if ( nodeModulesPattern.test( filename ) ) return original( m, filename );

	var source = fs.readFileSync( filename, 'utf-8' );

	var earlyExports = [];
	var lateExports = [];

	var compiled = tippex.replace( source, importPattern, replaceImports );

	compiled = tippex.replace( compiled, exportDeclarationPattern, ( match, type, name ) => {
		lateExports.push({ name, localName: name });
		return `${type} ${name}`;
	});

	compiled = tippex.replace( compiled, exportFunctionPattern, ( match, name ) => {
		earlyExports.push( name );
		return `function ${name}`;
	});

	compiled = tippex.replace( compiled, exportNamePattern, ( match, specifiers ) => {
		var statement = `${INTEROP_TEMP} = require('${source}');`;

		specifiers.split( ',' ).forEach( name => {
			name = name.trim();
			if ( !name ) return;

			var alias = aliasPattern.exec( name );
			lateExports.push({ name: alias[2] || alias[1], localName: alias[1] });
		});

		return '';
	});

	compiled = tippex.replace( compiled, exportDefaultPattern, () => 'exports.default = ' );

	compiled = earlyExports.map( name => `exports.${name} = ${name};` ).join( '\n' ) + '\n' + compiled;

	compiled += INTEROP;

	var exportStatements = lateExports.map( ({ name, localName }) => `exports.${name} = ${localName};` ).join( '\n' );
	compiled += exportStatements;

	m._compile( '"use strict";\n' + compiled, filename );
};
