var assert = require( 'assert' );
require( '..' );


describe( 'es-module-loader', () => {
	it( 'exports default', () => {
		assert.equal( require( './samples/export-default' ).default, 42 );
	});

	it( 're-exports default', () => {
		assert.equal( require( './samples/reexport-default' ).default, 42 );
	});

	it( 'exports a variable declaration', () => {
		assert.equal( require( './samples/export-var' ).answer, 42 );
	});

	it( 'exports a function', () => {
		assert.equal( require( './samples/export-function' ).answer(), 42 );
	});

	it( 'exports a name', () => {
		assert.equal( require( './samples/export-name' ).answer, 42 );
	});

	it( 'imports a namespace', () => {
		assert.equal( require( './samples/import-namespace' ).default, 'baz' );
	});
});
